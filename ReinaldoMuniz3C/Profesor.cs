﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuniz3C
{
    public abstract class Profesor
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Direccion { get; set; }
        public string Cedula { get; set; }

        public abstract void MostrarDatos();

        public abstract double CalcularSueldo();

    }
}
