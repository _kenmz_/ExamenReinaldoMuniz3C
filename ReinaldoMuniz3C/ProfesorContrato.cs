﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuniz3C
{
    class ProfesorContrato : Profesor
    {
        public double SueldoBasico { get; set; }
        public int HorasExtras { get; set; }

        public override void MostrarDatos()
        {
            Console.WriteLine("Profesor\nNombres: {0}\t\tApellidos: {1}\nDireccion: {2}\nCedula de identidad: {3}\n" +
                               "Modalidad: Contrato\nSueldo Básico: {4}\nCantidad de Horas Extras trabajadas: {5}\nSueldo Total: {6} $",
                               Nombres, Apellidos, Direccion, Cedula, SueldoBasico, HorasExtras, CalcularSueldo());
        }

        public override double CalcularSueldo()
        {
            double Sueldo = 0;
            double PrecioDia = SueldoBasico / 28;
            double PrecioHora = PrecioDia / 8;
            Sueldo = PrecioHora * HorasExtras;
            Sueldo = Math.Round(Sueldo * 100) / 100;
	    Sueldo += SueldoBasico;
            return Sueldo;
        }
    }
}
