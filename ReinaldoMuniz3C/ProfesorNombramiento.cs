﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuniz3C
{
    class ProfesorNombramiento : Profesor
    {
        public double Sueldo { get; set; }

        public override void MostrarDatos()
        {
            Console.WriteLine("Profesor\nNombres: {0}\t\tApellidos: {1}\nDireccion: {2}\nCedula de identidad: {3}\n" +
                               "Modalidad: Nombramiento\nSueldo: {4} $",
                               Nombres, Apellidos, Direccion, Cedula, Sueldo);
        }

        public override double CalcularSueldo()
        {
            return 0;
        }
    }
}
