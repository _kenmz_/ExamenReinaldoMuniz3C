﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuniz3C
{
    class ProfesorHora : Profesor
    {
        public double PrecioHora { get; set; }
        public int CantidadHoras { get; set; }

        public override void MostrarDatos()
        {
            Console.WriteLine("Profesor\nNombres: {0}\t\tApellidos: {1}\nDireccion: {2}\nCedula de identidad: {3}\n"+
                               "Modalidad: Por Hora\nPrecio la hora: {4}\nCantidad de Horas trabajadas: {5}\nSueldo: {6} $", Nombres, Apellidos, Direccion, Cedula, PrecioHora, CantidadHoras, CalcularSueldo());
        }

        public override double CalcularSueldo()
        {
            return PrecioHora * CantidadHoras;
        }

    }
}
