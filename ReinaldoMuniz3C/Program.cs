﻿using System;
using System.Collections.Generic;

namespace ReinaldoMuniz3C
{
    class Program
    {
        static void Main(string[] args)
        {
            ProfesorHora profesor1 = new ProfesorHora();
            profesor1.Nombres = "Malcolm Oliver";
            profesor1.Apellidos = "Wilkerson Pellicer";
            profesor1.Direccion = "calle CAntura nº 12334, Los Angeles";
            profesor1.Cedula = "1371829058";
            profesor1.PrecioHora = 2;
            profesor1.CantidadHoras = 16;
            profesor1.MostrarDatos();

            Console.WriteLine();

            ProfesorContrato profesor2 = new ProfesorContrato();
            profesor2.Nombres = "Marge Maria";
            profesor2.Apellidos = "Simpson Aranzazu";
            profesor2.Direccion = "Calle Siempre Viva 742, Springfield";
            profesor2.Cedula = "0905848587";
            profesor2.SueldoBasico = 399;
            profesor2.HorasExtras = 10;
            profesor2.MostrarDatos();

            Console.WriteLine();

            ProfesorNombramiento profesor3 = new ProfesorNombramiento();
            profesor3.Nombres = "Dwight Joel";
            profesor3.Apellidos = "Schrute Kaur";
            profesor3.Direccion = "Schrute farms, Scranton, Pensilvania";
            profesor3.Cedula = "1374372457";
            profesor3.Sueldo = 1224;
            profesor3.MostrarDatos();

            Console.WriteLine("\n--------------------------------------------------------------\n");

            List<Profesor> listaProfesores = new List<Profesor>();
            listaProfesores.Add(profesor1);
            listaProfesores.Add(profesor2);
            listaProfesores.Add(profesor3);

            foreach (Profesor profesor in listaProfesores) {
                profesor.MostrarDatos();
                Console.WriteLine(profesor.CalcularSueldo()+"\n");
            }


        }
    }
}
